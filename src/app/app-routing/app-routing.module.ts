import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {CivilLitigationComponent} from '../pages/civil-litigation/civil-litigation.component';
import {CustodyComponent} from '../pages/custody/custody.component';
import {DivorceComponent} from '../pages/divorce/divorce.component';
import {RouterModule} from '@angular/router';
import {ContactComponent} from '../pages/contact/contact.component';
import {HomeComponent} from '../pages/home/home.component';
import {NickMermigesComponent} from '../pages/nick-mermiges/nick-mermiges.component';
import {PapersComponent} from '../pages/resources/papers/papers.component';
import {PersonalInjuryComponent} from '../pages/personal-injury/personal-injury.component';
import {ResourcesComponent} from '../pages/resources/resources.component';
import {VideosComponent} from '../pages/resources/videos/videos.component';

const pageBase = 'page/';
const routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomeComponent,
    },
    {
        path: pageBase + 'contact',
        component: ContactComponent
    },
    {
        path: pageBase + 'nick_mermiges',
        component: NickMermigesComponent
    },
    {
        path: pageBase + 'divorce',
        component: DivorceComponent
    },
    {
        path: pageBase + 'custody',
        component: CustodyComponent
    },
    {
        path: pageBase + 'personal',
        component: PersonalInjuryComponent
    },
    {
        path: pageBase + 'civil',
        component: CivilLitigationComponent
    },
    {
        path: pageBase + 'resources',
        component: ResourcesComponent
    },
    {
        path: pageBase + 'resources/papers/:paper',
        component: PapersComponent
    },
    {
        path: pageBase + 'resources/videos/:video',
        component: VideosComponent
    },
    {
        path: '**',
        component: HomeComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
