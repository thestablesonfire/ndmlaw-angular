import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavigationComponent } from './global/navigation/navigation.component';
import { HeroComponent } from './global/hero/hero.component';
import {PageLinkService} from './global/page-link.service';
import { HomeComponent } from './pages/home/home.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import { FooterComponent } from './global/footer/footer.component';
import { NickMermigesComponent } from './pages/nick-mermiges/nick-mermiges.component';
import { ContactComponent } from './pages/contact/contact.component';
import { DivorceComponent } from './pages/divorce/divorce.component';
import { CustodyComponent } from './pages/custody/custody.component';
import { PersonalInjuryComponent } from './pages/personal-injury/personal-injury.component';
import { CivilLitigationComponent } from './pages/civil-litigation/civil-litigation.component';
import { ResourcesComponent } from './pages/resources/resources.component';
import {PapersComponent} from './pages/resources/papers/papers.component';
import {VideosService} from './pages/resources/videos.service';
import { VideosComponent } from './pages/resources/videos/videos.component';
import { HighAssetDivorceComponent } from './pages/resources/papers/high-asset-divorce/high-asset-divorce.component';
import { GrandparentCustodyComponent } from './pages/resources/papers/grandparent-custody/grandparent-custody.component';
import { PrenupPaperComponent } from './pages/resources/papers/prenup-paper/prenup-paper.component';
import { DuiLawPaperComponent } from './pages/resources/papers/dui-law-paper/dui-law-paper.component';

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        HeroComponent,
        HomeComponent,
        FooterComponent,
        NickMermigesComponent,
        ContactComponent,
        DivorceComponent,
        CustodyComponent,
        PersonalInjuryComponent,
        CivilLitigationComponent,
        ResourcesComponent,
        PapersComponent,
        VideosComponent,
        HighAssetDivorceComponent,
        GrandparentCustodyComponent,
        PrenupPaperComponent,
        DuiLawPaperComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [
        PageLinkService,
        VideosService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
