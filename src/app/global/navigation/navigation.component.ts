import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Link} from '../link';
import {PageLinkService} from '../page-link.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NavigationComponent implements OnInit {
    links: Link[];

    constructor(private linkService: PageLinkService) {
        this.links = [
            new Link('Who We Are', linkService.getPageLink('nick_mermiges')),
            new Link('What We Practice', '0', [
                new Link('Divorce', linkService.getPageLink('divorce')),
                new Link('Custody', linkService.getPageLink('custody')),
                new Link('Personal Injury', linkService.getPageLink('personal')),
                new Link('Civil Litigation', linkService.getPageLink('civil')),
            ]),
            new Link('Resources', linkService.getPageLink('resources')),
            new Link('Contact', linkService.getPageLink('contact'))
        ];
    }

    ngOnInit() {
    }
}
