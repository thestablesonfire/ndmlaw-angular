import { TestBed, inject } from '@angular/core/testing';

import { PageLinkService } from './page-link.service';

describe('PageLinkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PageLinkService]
    });
  });

  it('should be created', inject([PageLinkService], (service: PageLinkService) => {
    expect(service).toBeTruthy();
  }));
});
