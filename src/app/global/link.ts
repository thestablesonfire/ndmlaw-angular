export class Link {
    constructor(public text: string, public href: string, public sublinks?: Link[]) {}
}
