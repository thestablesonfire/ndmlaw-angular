import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PageLinkService} from '../page-link.service';

@Component({
    selector: 'app-hero',
    templateUrl: './hero.component.html',
    styleUrls: ['./hero.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HeroComponent implements OnInit {
    constructor(public linkService: PageLinkService) {}

    ngOnInit() {}
}
