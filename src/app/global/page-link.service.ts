import { Injectable } from '@angular/core';

@Injectable()
export class PageLinkService {
    pageBase: string;

    constructor() {
        this.pageBase = 'page/';
    }

    /**
     * Returns the URI for a page
     * @param pageName
     * @return {string}
     */
    getPageLink(pageName) {
        return this.pageBase + pageName;
    }
}
