import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {VideosService} from '../videos.service';

@Component({
    selector: 'app-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
    /**
     * The video to be displayed
     */
    public activeVideo: any;

    /**
     * Displays video resources
     * @param {VideosService} videosService
     * @param {ActivatedRoute} route
     * @param {DomSanitizer} sanitizer
     */
    constructor(
        private videosService: VideosService,
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer
    ) {
        this.activeVideo = videosService.getVideo(this.route.snapshot.paramMap.get('video'));
    }

    /**
     * Returns a safe, sanitized url for displaying the videos
     * @return {SafeResourceUrl}
     */
    getUrl() {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.activeVideo.url);
    }

    ngOnInit() {}
}
