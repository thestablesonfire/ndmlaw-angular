import { Component, OnInit } from '@angular/core';
import {Resource} from '../../resource';

@Component({
    selector: 'app-resources',
    templateUrl: './resources.component.html',
    styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {
    videos: Resource[];
    papers: Resource[];

    constructor() {
        this.videos = this.getVideos();
        this.papers = this.getPapers();
    }

    ngOnInit() {
    }

    resourceClick(resource: Resource) {
        console.log(resource);
    }

    private getPapers() {
        return [
            new Resource(
                '9/10/2015',
                'High Asset & Complex Divorces in South Carolina.',
                'high_asset_divorce'
            ),
            new Resource(
                '4/02/2015',
                'South Carolina Statutes and Case Law regarding Grandparents\' Rights to Custody and Visitation in South Carolina.',
                'grandparent_custody'
            ),
            new Resource(
                '3/25/2015',
                'South Carolina Statutes and Case Law regarding Scope and Execution of Prenuptial Agreements.',
                'prenup_paper'
            ),
            new Resource(
                '3/20/2015',
                'South Carolina DUI Laws and Penalties.',
                'dui_law'
            )
        ];
    }

    /**
     * Returns the list of videos
     * @return {Resource[]}
     */
    private getVideos() {
        return [
            new Resource(
                '10/21/2015',
                'DUI Defense: Challenging BAC Readings from Datamaster.',
                'bac_readings_video'
            ),
            new Resource(
                '9/22/2015',
                'Grounds for Divorce South Carolina and Evidence Needed to Prove Those Grounds.',
                'divorce_grounds_video'
            ),
            new Resource(
                '9/22/2015',
                'Motions for Temporary Relief in SC Family Court.',
                'family_court_video'
            ),
            new Resource(
                '3/26/2015',
                'Drafting and Enforcing South Carolina Prenuptial Agreements.',
                'prenup_video'
            ),
        ];
    }
}
