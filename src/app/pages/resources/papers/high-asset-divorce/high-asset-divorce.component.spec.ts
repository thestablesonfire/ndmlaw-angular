import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighAssetDivorceComponent } from './high-asset-divorce.component';

describe('HighAssetDivorceComponent', () => {
  let component: HighAssetDivorceComponent;
  let fixture: ComponentFixture<HighAssetDivorceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighAssetDivorceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighAssetDivorceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
