import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuiLawPaperComponent } from './dui-law-paper.component';

describe('DuiLawPaperComponent', () => {
  let component: DuiLawPaperComponent;
  let fixture: ComponentFixture<DuiLawPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuiLawPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuiLawPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
