import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrandparentCustodyComponent } from './grandparent-custody.component';

describe('GrandparentCustodyComponent', () => {
  let component: GrandparentCustodyComponent;
  let fixture: ComponentFixture<GrandparentCustodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrandparentCustodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrandparentCustodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
