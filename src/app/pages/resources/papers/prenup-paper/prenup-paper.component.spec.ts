import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenupPaperComponent } from './prenup-paper.component';

describe('PrenupPaperComponent', () => {
  let component: PrenupPaperComponent;
  let fixture: ComponentFixture<PrenupPaperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrenupPaperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrenupPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
