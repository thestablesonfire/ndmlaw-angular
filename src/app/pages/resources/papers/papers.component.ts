import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-papers',
    templateUrl: './papers.component.html',
    styleUrls: ['./papers.component.scss']
})
export class PapersComponent implements OnInit {
    activePaper: string;

    constructor(
        private route: ActivatedRoute,
    ) {
        this.activePaper = this.route.snapshot.paramMap.get('paper');
        console.log(this.activePaper);
    }

    displayPaper(id: string) {
        console.log(id, this.activePaper);
        return this.activePaper === id;
    }

    ngOnInit() {
    }

}
