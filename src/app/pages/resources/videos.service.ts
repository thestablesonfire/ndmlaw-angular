import { Injectable } from '@angular/core';

@Injectable()
export class VideosService {
    videos: any[];

    constructor() {
        this.videos = [
            {
                id: 'bac_readings_video',
                url: '//www.youtube.com/embed/550Y-4ci6Sk'
            },
            {
                id: 'divorce_grounds_video',
                url: '//www.youtube.com/embed/ACifYrNFvJI'
            },
            {
                id: 'family_court_video',
                url: '//www.youtube.com/embed/bALW6H5GitM'
            },
            {
                id: 'prenup_video',
                url: '//www.youtube.com/embed/n3vBlhE8Qco'
            }
        ];
    }

    getVideo(videoId: string) {
        return this.videos.find(function (video) {
            return video.id === videoId;
        });
    }
}
