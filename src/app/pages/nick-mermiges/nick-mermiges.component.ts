import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-nick-mermiges',
    templateUrl: './nick-mermiges.component.html',
    styleUrls: ['./nick-mermiges.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class NickMermigesComponent implements OnInit {
    nickImage: string;
    constructor() {
        this.nickImage = 'assets/images/img_hero.png';
    }

    ngOnInit() {}
}
