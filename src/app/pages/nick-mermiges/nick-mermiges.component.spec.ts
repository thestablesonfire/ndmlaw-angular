import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NickMermigesComponent } from './nick-mermiges.component';

describe('NickMermigesComponent', () => {
  let component: NickMermigesComponent;
  let fixture: ComponentFixture<NickMermigesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NickMermigesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NickMermigesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
