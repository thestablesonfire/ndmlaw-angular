import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-personal-injury',
    templateUrl: './personal-injury.component.html',
    styleUrls: ['./personal-injury.component.scss']
})
export class PersonalInjuryComponent implements OnInit {
    heroImage: string;

    constructor() {
        this.heroImage = 'assets/images/img_personal_injury.png';
    }

    ngOnInit() {
    }

}
