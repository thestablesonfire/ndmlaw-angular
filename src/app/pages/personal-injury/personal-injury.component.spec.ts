import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalInjuryComponent } from './personal-injury.component';

describe('PersonalInjuryComponent', () => {
  let component: PersonalInjuryComponent;
  let fixture: ComponentFixture<PersonalInjuryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalInjuryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalInjuryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
