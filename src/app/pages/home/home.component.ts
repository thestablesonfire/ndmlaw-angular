import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {PageLinkService} from '../../global/page-link.service';
import {AboutItem} from '../about-item';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
    aboutItems: AboutItem[];
    nickHero: string;

    constructor(public linkService: PageLinkService) {
        const imageBase = 'assets/images/';
        this.nickHero = imageBase + 'img_hero.png';
        this.aboutItems = [
            new AboutItem('Divorce', imageBase + 'img_family_law.png', 'divorce'),
            new AboutItem('Custody', imageBase + 'img_custody.png', 'custody'),
            new AboutItem('Personal Injury', imageBase + 'img_personal_injury.png', 'personal'),
            new AboutItem('Civil Litigation', imageBase + 'img_civil_litigation.png', 'civil')
        ];
    }

    ngOnInit() {}
}
