import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CivilLitigationComponent } from './civil-litigation.component';

describe('CivilLitigationComponent', () => {
  let component: CivilLitigationComponent;
  let fixture: ComponentFixture<CivilLitigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CivilLitigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CivilLitigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
