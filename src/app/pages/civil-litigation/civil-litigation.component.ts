import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-civil-litigation',
    templateUrl: './civil-litigation.component.html',
    styleUrls: ['./civil-litigation.component.scss']
})
export class CivilLitigationComponent implements OnInit {
    heroImage: string;

    constructor() {
        this.heroImage = 'assets/images/img_civil_litigation.png';
    }

    ngOnInit() {
    }

}
