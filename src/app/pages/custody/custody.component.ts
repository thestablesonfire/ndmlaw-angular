import { Component, OnInit } from '@angular/core';
import {PageLinkService} from '../../global/page-link.service';

@Component({
    selector: 'app-custody',
    templateUrl: './custody.component.html',
    styleUrls: ['./custody.component.scss']
})
export class CustodyComponent implements OnInit {
    heroImage: string;

    constructor(public linkService: PageLinkService) {
        this.heroImage = 'assets/images/img_custody.png';
    }

    ngOnInit() {
    }

}
