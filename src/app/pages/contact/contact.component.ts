import {HttpClient} from '@angular/common/http';
import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ContactComponent implements OnInit {
    mailerLocation: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    message: string;

    constructor(private http: HttpClient) {
        this.mailerLocation = 'assets/php/mailer.php';
    }

    ngOnInit() {
    }

    onFormSubmit() {
        console.log(this.firstName, this.lastName, this.email, this.phoneNumber, this.message);

        this.http.post(this.mailerLocation, {
            fname: this.firstName,
            lname: this.lastName,
            phone: this.phoneNumber,
            email: this.email,
            message: this.message
        }).subscribe(response => {
            console.log(response);
        });
    }
}
