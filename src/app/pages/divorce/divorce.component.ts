import { Component, OnInit } from '@angular/core';
import {PageLinkService} from '../../global/page-link.service';

@Component({
    selector: 'app-divorce',
    templateUrl: './divorce.component.html',
    styleUrls: ['./divorce.component.scss']
})
export class DivorceComponent implements OnInit {
    heroImage: string;

    constructor(public linkService: PageLinkService) {
        this.heroImage = 'assets/images/img_family_law.png';
    }

    ngOnInit() {
    }

    resourcesLink() {
        return this.linkService.getPageLink('resources');
    }
}
