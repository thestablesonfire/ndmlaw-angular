export class AboutItem {
    constructor(public title: string, public image: string, public page: string) {}
}
