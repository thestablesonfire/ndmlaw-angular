export class Resource {
    constructor(public date: string, public description: string, public id: string) {}
}
